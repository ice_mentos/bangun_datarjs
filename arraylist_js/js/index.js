let inputDropdownDOM = document.getElementById('number')
let bangunDatar = ['Lingkaran', 'Persegi', 'Segitiga', 'Trapesium', 'Jajargenjang'];

let inputDOM = document.getElementById('number')
let btnSaveDOM = document.getElementById('btn-save')
let inputPDOM = document.getElementById('number-persegi')
let alasDOM = document.getElementById('alas')
let tinggiDOM = document.getElementById('tinggi')
let miringDOM = document.getElementById('miring')
let diameterDOM = document.getElementById('diameter')
let sisi1DOM = document.getElementById('sisi1')
let sisi2DOM = document.getElementById('sisi2')
let sisi3DOM = document.getElementById('sisi3')
let sisi4DOM = document.getElementById('sisi4')


function segitiga(){
    let keliling = parseFloat(alasDOM.value) + parseFloat(tinggiDOM.value) + parseFloat(miringDOM.value);
    let luas = parseFloat(alasDOM.value)*parseFloat(tinggiDOM.value) / 2;
    document.getElementById('keliling').value = keliling;
    document.getElementById('luas').value = luas; 
    console.log(keliling);
    console.log(luas);
}

function persegi(){
    let hasilKeliling = parseFloat(inputPDOM.value)*4;
    let hasilLuas = parseFloat(inputPDOM.value)*parseFloat(inputPDOM.value);
    document.getElementById('keliling').value = hasilKeliling;
    document.getElementById('luas').value = hasilLuas;
    console.log(hasilKeliling);
    console.log(hasilLuas);
}

function lingkaran(){
    let keliling = parseFloat(diameterDOM.value)*3.14;
    let luas = parseFloat(diameterDOM.value)*3.14/4;
    document.getElementById('keliling').value = keliling;
    document.getElementById('luas').value = luas;
}

function trapesium(){
    let keliling = parseFloat(sisi1DOM.value) + parseFloat(sisi2DOM.value) + parseFloat(sisi3DOM.value) + parseFloat(sisi4DOM.value)
    let luas = (parseFloat(sisi1DOM.value) + parseFloat(sisi2DOM.value)) * parseFloat(tinggiDOM.value) / 2
    document.getElementById('keliling').value = keliling
    document.getElementById('luas').value = luas
}

function jajargenjang(){
    let keliling = 2 * (parseFloat(alasDOM.value) + parseFloat(miringDOM.value))
    let luas = parseFloat(alasDOM.value)*parseFloat(tinggiDOM.value)
    document.getElementById('keliling').value = keliling
    document.getElementById('luas').value = luas
}